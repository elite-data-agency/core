# EDA Core

[![pipeline status](https://gitlab.com/elite-data-agency/core/badges/main/pipeline.svg)](https://gitlab.com/elite-data-agency/core/-/commits/main)
[![coverage report](https://gitlab.com/elite-data-agency/core/badges/main/coverage.svg)](https://gitlab.com/elite-data-agency/core/-/commits/main)

The core of EDA is the data ingestion and projection methods. This system consumes data from multiple sources
and projects them into various formats and datastores for later consumption.

EDA Core aims to be cross compilation compatible (practically speaking, you should always be able to build with `CGO_ENABLED=0`).

## Current Sources

- [EDDN (The OG)](https://eddn.edcd.io/)

### Planned

- Inara
- EDA Pilot Implant
- Others?

# Development

All commit messages should follow [Conventional Commits](https://www.conventionalcommits.org) format.

## Getting Started

Copy `example.env` to `.env` and modify the values for your local environment. When running the application in a
production environment, you should set the configuration via environment variables.
