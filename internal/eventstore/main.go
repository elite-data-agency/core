package eventstore

import (
	"context"
	"strings"
	"time"

	"github.com/go-redis/redis/v8"
	"github.com/spf13/viper"
	"gitlab.com/elite-data-agency/core/internal/eddn"
	"gitlab.com/elite-data-agency/core/internal/logging"
	"go.uber.org/zap"
)

var (
	client *redis.Client
	ctx    context.Context
	log    *zap.SugaredLogger
)

func Initialize() {
	log = logging.SugarFor("eventstore")
	ctx = context.TODO() // This should be dependent on the loading method

	redisOpts, err := redis.ParseURL(viper.GetString("REDIS_URL"))

	if err != nil {
		log.Panicw(
			"unable to parse REDIS_URL",
			"url", viper.Get("REDIS_URL"),
		)
	}

	client = redis.NewClient(redisOpts)

	_, err = client.Ping(ctx).Result()

	if err != nil {
		log.Panicw(
			"unable to connect to redis",
			"host", redisOpts.Addr,
			"username", redisOpts.Username,
			"error", err,
		)
	}
}

func Publish(message *eddn.EDDN) error {
	err := client.XAdd(ctx, &redis.XAddArgs{
		Stream: strings.Replace(message.SchemaRef, "https://eddn.edcd.io/", "", 1),
		MaxLen: 0,
		Approx: true,
		ID:     "",
		Values: map[string]interface{}{
			"swName": string(message.Header.SoftwareName),
			"swVer":  string(message.Header.SoftwareVersion),
			"srcID":  string(message.Header.UploaderID),
			"gwTS":   string(message.Header.GatewayTimestamp.Format(time.RFC3339)),
			"data":   string(message.Message),
		},
	}).Err()

	if err != nil {
		log.DPanicw(
			"publishing failed",
			"err", err,
		)
	}

	log.Debugw(
		"message published",
		"rawData", message.Message,
	)
	return err
}
