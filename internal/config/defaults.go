package config

import (
	"github.com/spf13/viper"
)

const (
	AppEnvVarName = "EDA_ENV"
)

// We shouldn't be using too many defaults here. We should prefer
// to default to the %environment%.env file for defaults.

func getRequiredKeys() []string {
	return []string{"EDDN_ENDPOINT", AppEnvVarName, "REDIS_URL"}
}

func setDefaults() {
	viper.SetDefault(AppEnvVarName, "development")
	viper.SetDefault("EDDN_ENDPOINT", "tcp://eddn.edcd.io:9500")
	viper.SetDefault("REDIS_URL", "redis://localhost:6379/0")
}

func validateRequired() bool {
	for _, key := range getRequiredKeys() {
		if !viper.IsSet(key) {
			//TODO: Chicken/Egg problem with config and logger.
			// Need to optimize the configs needed for logging and eager load that
			log.Fatalf("The required configuration paramenter '%s' is missing. Please read the documentation and provide a valid value.", key)
		}
	}
	return true
}
