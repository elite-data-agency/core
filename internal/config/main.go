package config

import (
	"fmt"
	"io/fs"
	"os"

	"github.com/spf13/viper"
	"gitlab.com/elite-data-agency/core/internal/logging"
	"go.uber.org/zap"
)

var (
	log *zap.SugaredLogger
)

func Initialize() {
	log = logging.SugarFor("config")
	setDefaults()

	viper.SetConfigType("env")
	viper.AddConfigPath(".")

	viper.SetConfigFile(fmt.Sprintf("%s.env", os.Getenv(AppEnvVarName)))

	if err := viper.ReadInConfig(); err != nil {
		if _, ok := err.(*fs.PathError); ok {
			log.Warnw(
				"no configuration file found, continuing with environment variables only",
				"env", viper.Get(AppEnvVarName),
			)
		} else {
			panic(err)
		}
	}

	viper.SetConfigFile(".env")
	if err := viper.MergeInConfig(); err != nil {
		log.Debugw(
			"no .env file found",
			"error", err,
		)
	}

	validateRequired()

	log.Debugw(
		"configuration loaded",
		"env", viper.GetString(AppEnvVarName),
		"log_level", viper.GetString("LOG_LEVEL"),
		"eddn_endpoint", viper.GetString("EDDN_ENDPOINT"),
		"redis_url", viper.Get("REDIS_URL"),
		"config_file", viper.ConfigFileUsed(),
	)
}
