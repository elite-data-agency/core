package logging

import (
	"fmt"
	"os"

	"github.com/spf13/viper"
	"go.uber.org/zap"
	"go.uber.org/zap/zapcore"
)

var (
	logger *zap.Logger
)

func Initialize() {
	atom := zap.NewAtomicLevel()
	err := atom.UnmarshalText([]byte(viper.GetString("LOG_LEVEL")))

	encoderCfg := zap.NewProductionEncoderConfig()

	logger = zap.New(zapcore.NewCore(
		zapcore.NewJSONEncoder(encoderCfg),
		zapcore.Lock(os.Stdout),
		atom,
	),
		zap.AddCaller(),
		zap.AddStacktrace(zapcore.ErrorLevel),
	)

	if viper.GetString("EDA_ENV") == "development" {
		logger = logger.WithOptions(
			zap.Development(),
		)
	}

	if err != nil {
		panic(fmt.Sprintf("Unable to instantiate logger, you should panic (we are)!\n %v", err))
	}
	defer logger.Sync()

	zap.ReplaceGlobals(logger)
	zap.RedirectStdLog(logger)

	logger.Sugar().Debugw(
		"logger configured",
		"env_level", viper.GetString("LOG_LEVEL"),
		"zap_level", atom.Level().String(),
	)
}

func SugarFor(name string) *zap.SugaredLogger {
	return logger.Sugar().Named(name)
}
