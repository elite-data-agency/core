package eddn

import (
	"bytes"
	"compress/zlib"
	"context"
	"encoding/json"
	"fmt"
	"time"

	"github.com/go-zeromq/zmq4"
	"github.com/spf13/viper"
	"gitlab.com/elite-data-agency/core/internal/logging"
	"go.uber.org/zap"
)

type EDDN struct {
	SchemaRef string          `json:"$schemaRef"`
	Header    EDDNHeader      `json:"header"`
	Message   json.RawMessage `json:"message"`
}
type EDDNHeader struct {
	UploaderID       string    `json:"uploaderID"`
	SoftwareName     string    `json:"softwareName"`
	SoftwareVersion  string    `json:"softwareVersion"`
	GatewayTimestamp time.Time `json:"gatewayTimestamp"`
}

var (
	log *zap.SugaredLogger
)

func Initialize(receiver func(*EDDN)) {
	log = logging.SugarFor("eddn")

	sub := zmq4.NewSub(context.Background())
	defer sub.Close()

	err := sub.Dial(viper.GetString("EDDN_ENDPOINT"))
	if err != nil {
		log.DPanicw(
			fmt.Sprintf("could not connect to EDDN at %v", viper.GetString("EDDN_ENDPOINT")),
		)
	}

	err = sub.SetOption(zmq4.OptionSubscribe, "")
	if err != nil {
		log.DPanicw(
			fmt.Sprintf("could not subscribe: %v", err),
			"error", err,
		)
	}

	log.Infow(
		"connected to eddn",
		"endpoint", viper.Get("EDDN_ENDPOINT"),
	)

	for {
		msg, err := sub.Recv()
		if err != nil {
			log.DPanicw(
				"unable to receive message",
				"error", err,
			)
		}

		message, err := messageDecoder(msg.Bytes())

		if err != nil {
			log.DPanicw(
				"unable to decode message",
				"msg_bytes", msg.Bytes(),
				"error", err.Error(),
			)
			continue
		}

		receiver(message)
	}
}

func messageDecoder(rawMessage []byte) (*EDDN, error) {
	r, err := zlib.NewReader(bytes.NewReader(rawMessage))
	if err != nil {
		return nil, err
	}
	defer r.Close()

	var message EDDN
	err = json.NewDecoder(r).Decode(&message)
	if err != nil {
		return nil, err
	}

	return &message, nil
}
