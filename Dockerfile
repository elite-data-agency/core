FROM golang:1.17 as builder
WORKDIR /go/src/app
COPY . .
RUN CGO_ENABLED=0 go install -ldflags '-extldflags "-static"' -tags timetzdata ./cmd/*

FROM scratch
# the test program:
COPY --from=builder /go/bin/* /
COPY --from=alpine:latest /etc/ssl/certs/ca-certificates.crt /etc/ssl/certs/
CMD ["/api"]
EXPOSE 1323