package main

import (
	"encoding/json"

	"github.com/spf13/viper"
	"gitlab.com/elite-data-agency/core/internal/config"
	"gitlab.com/elite-data-agency/core/internal/eddn"
	"gitlab.com/elite-data-agency/core/internal/eventstore"
	"gitlab.com/elite-data-agency/core/internal/logging"
	"go.uber.org/zap"
)

var (
	log *zap.SugaredLogger
)

func init() {
	viper.BindEnv("LOG_LEVEL")

	logging.Initialize()
	log = logging.SugarFor("cmd.ingest")

	config.Initialize()
}

func main() {
	eventstore.Initialize()
	eddn.Initialize(storeMessage)
}

func storeMessage(message *eddn.EDDN) {
	_, err := json.Marshal(&message.Message)
	if err != nil {
		log.DPanicw("unable to unmarshal message",
			"message", message,
			"error", err,
		)
	}

	log.Infow(
		"message stored",
		"schema", message.SchemaRef[len("https://eddn.edcd.io/"):],
	)
	eventstore.Publish(message)
}
